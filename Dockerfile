FROM centos:7.7.1908

RUN set -ex;                                                               \
    yum update --assumeyes;                                                \
    yum upgrade --assumeyes;                                                \
    yum install --assumeyes                                                \
        elfutils \
        ksh \
        gettext \
        ImageMagick \
        rsyslog \
        bash \
        expat \
        ntp \
        polkit \
        http-parser \
        systemd \
        patch;                                                             \
